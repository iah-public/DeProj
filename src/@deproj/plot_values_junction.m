function hts = plot_values_junction( obj, values, ax )
%PLOT_VALUES_JUNCTION Plot the tissue with the cell contour approximated by the junctions, colored by the specified values.

    [ V, F ] = obj.graph_to_VF();

    n_objects = numel( obj.epicells );
    if n_objects > 1000, lw = 1; else, lw = 2; end
    
    
    if isempty( values )

        hts = patch( ...
            'Faces', F, ...
            'Vertices', V, ...
            'FaceColor', 'none', ...
            'EdgeColor', 'k', ...
            'LineWidth', lw, ...
            'Parent', ax );
        
    elseif ischar( values )
        
        hts = patch( ...
            'Faces', F, ...
            'Vertices', V, ...
            'FaceColor', 'none', ...
            'EdgeColor', values, ...
            'LineWidth', lw, ...
            'Parent', ax );

    else
        
        hts = patch( ...
            'Faces', F, ...
            'Vertices', V, ...
            'FaceVertexCData', values, ...
            'FaceColor', 'flat', ...
            'LineWidth', lw, ...
            'Parent', ax );
        
    end

end

